#!/bin/sh

set -e

systemctl start king-phisher

if ! systemctl is-active -q king-phisher; then
    echo "The service fails to start"
    exit 1    
fi

cd /usr/share/king-phisher && ./KingPhisher -h

systemctl stop king-phisher
